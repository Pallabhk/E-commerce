

<!-- Page container -->
		
		<div class="page-container">

			<!-- Page content -->
		    <div class="page-content">

				<!-- Main sidebar -->
				<div class="sidebar sidebar-main">
					<div class="sidebar-content">

							<!-- User menu -->
							<div class="sidebar-user">
								<div class="category-content">
									<div class="media">
										<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
										<div class="media-body">
											<span class="media-heading text-semibold">
												<?php 
													if(isset($_SESSION['r_admin_name'])){
														echo $_SESSION['r_admin_name']." **Root Admin";
													}else if(isset($_SESSION['admin_name'])){
														echo $_SESSION['admin_name']." **Admin";
													}
												?>
											</span>
											<div class="text-size-mini text-muted">
												<i class="icon-pin text-size-small"></i> &nbsp;Dhaka, Bangladesh
											</div>
										</div>

										<div class="media-right media-middle">
											<ul class="icons-list">
												<li>
													<a href="#"><i class="icon-cog3"></i></a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<!-- /user menu -->


						<!-- Main navigation -->
						<div class="sidebar-category sidebar-category-visible">
							<div class="category-content no-padding">
								<ul class="navigation navigation-main navigation-accordion">

									<!-- Main -->
									<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
									<li class="active"><a href="index.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
									<li>
										<a href="#"><i class="icon-stack2"></i> <span>Add Caterory</span></a>
										<ul>
											<li><a href="add_category.php">Add Caterory</a></li>
											<li><a href="category_table.php">Caterory Table</a></li>
										</ul>
									</li>
									<li>
										<a href="#"><i class="icon-stack2"></i> <span>Product</span></a>
										<ul>
											<li><a href="addProduct.php">Add Product</a></li>
											<li><a href="addProduct.php">Manage Product</a></li>
										</ul>
									</li>
									<li>
										<a href="add_admin.php"><i class="icon-stack2"></i> <span>Add Admin</span></a>
										<ul>
											<li class="<?php if(basename($_SERVER['PHP_SELF'])=='root_admin.php'){ echo 'active'; }elseif(basename($_SERVER['PHP_SELF'])=='admin.php'){ echo 'disabled'; }?>"><a href="add_admin.php">Add Admin</a></li>
											<li class="<?php if(basename($_SERVER['PHP_SELF'])=='root_admin.php'){ echo 'active'; }elseif(basename($_SERVER['PHP_SELF'])=='admin.php'){ echo 'disabled'; }?>"><a href="adminlist.php">Manage Admin</a></li>
										</ul>
									</li>
								

								</ul>
							</div>
						</div>
					<!-- /main navigation -->

					</div>
				</div><!-- /main sidebar -->

				<!-- Main content -->
				<div class="content-wrapper">

					<!-- Page header -->
					<div class="page-header">
						<div class="page-header-content">
							<div class="page-title">
								<h3 class="text-center">
									<!--Admin Name -->

									<?php
										
										if (isset($_SESSION['r_admin_name'])){
											echo $_SESSION['r_admin_name'] ."  **ROOT ADMIN";
										}elseif (isset($_SESSION['admin_name'])) {
											echo  $_SESSION['admin_name']. "  Admin";
										}
									?>


								</h3>
							</div>
						</div>

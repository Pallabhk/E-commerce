<?php include('header.php');?>
<?php include('sidebar.php');?>

<?php

	require_once('../class_lib/add_product_class.php');

	$last_id_obj= new Add_Product;
	$last_id=$last_id_obj->last_id();
?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">PRODUCT - ADD</span> || <a href="#"> PRODUCT</a></h4>
			</div>	
		</div>
	</div>
</div>
<!-- Add about terms -->

	
  			<div class="container-fluid">
  				<?php
				if(isset($_POST['prod_submit']) && !empty($_POST['prod_categ'])){
					require_once('../class_lib/add_product_class.php');
					$prod_insert_obj= new Add_Product;
					$prod_insert_obj->product_insert($_POST);
					}

				?>
		    <!-- about basic info about module -->
			    <h3 class="alert alert-info text-center">Add New Product</h3>
			  	<input id="product_serial" class="sr-only" value="<?php echo $last_id; ?>">
			   <?php
			   			require_once('../class_lib/maincatclass.php');

			    		$main_cat_obj  = new Main_Category;

			    		$main_cat_data =$main_cat_obj->main_categ_view();
			 
			   ?>
				<form method="post" enctype="multipart/form-data" class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
				  <div class="form-group">
					<label for="prod_categ">Choose Product Category</label>
					<select name="prod_categ" class="form-control" id="prod_categ">
						<option value="">Choose Product Category</option>
				<?php
					if($main_cat_data->num_rows>0){
						while($main_cat_list= $main_cat_data->fetch_assoc()){
							
							$main_cat_name=$main_cat_list['main_categ_name'];
							$main_cat_value=$main_cat_list['main_categ_folder'];
							
							echo '<option disabled="disabled" style="font-weight:700; background-color:skyblue; border-bottom:1px solid black;">'.$main_cat_name.'</option>';
							
							############# Sub Category view	#############

								require_once('../class_lib/sub_category_class.php');
								$sub_cat_obj= new Sub_Category;
								$sub_category_table=$sub_cat_obj->sub_categ_view_main($main_cat_value);
								
								if($sub_category_table->num_rows > 0){
									while($sub_categ_list=$sub_category_table->fetch_assoc()){
										$sub_categ_name=$sub_categ_list['sub_categ_name'];
										$sub_categ_value=$sub_categ_list['sub_categ_folder'];
										///print_r($sub_categ_list);
										
										echo '<option value="'.$main_cat_value.'#'.$sub_categ_value.'">'.$sub_categ_name.' of '.$main_cat_name.'</option>';
										
									}////// While Loop							
								}
								
						}
					}else{
						echo '<option value="">There have no Main Category</option>';
					}
				
				?>
					</select>
				  </div>
				  <div class="form-group">
					<label for="prod_code">Product Code</label>
					<input name="prod_code" type="text" class="form-control" id="prod_code" value="<?php echo date('Ymd')."-".$last_id; ?>" placeholder="Product Code">
				  </div>
				  <div class="form-group">
					<label for="prod_name">Product Name</label>
					<input name="prod_name" type="text" class="form-control" id="prod_name"  placeholder="Product Name">
				  </div>
				  <div class="form-group">
					<label for="prod_price">Product Price</label>
					<input name="prod_price" type="text" class="form-control" id="prod_price"  placeholder="Product Price">
				  </div>
				  <div class="form-group">
					<label for="prod_desc">Product Description</label>
					<textarea name="prod_desc" type="text" rows="3" class="form-control" id="prod_desc"  placeholder="Product Description"></textarea>
				  </div>
				  <div class="form-group">
					<label for="prod_img">Product Image</label>
					<input type="file" name="prod_img" id="prod_img">
				  </div>
				  <div class="form-group">
				  <button name="prod_submit" type="submit" class="btn btn-primary">Product Submit</button>
				  </div>
				</form>
		   </div>
		<br/>


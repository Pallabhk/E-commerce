<?php include('header.php');?>
<?php include('sidebar.php');?>

 	
	



<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">CATEGORY LIST</span> || <a href="#">MAIN CATEGORY</a></h4>
			</div>	
		</div>
	</div>
</div>
<!-- Add about terms -->
	
  			<div class="container-fluid">
		    <!-- about basic info about module -->
			    <h3 class="alert alert-info text-center">Category Table</h3>
			  
				
				<table class="table table-striped">
					<tr>
						<th>sl</th>
						<th>Main Category</th>
						<th>Sub Category</th>
					</tr>
					<?php
						require_once('../class_lib/maincatclass.php');

						$main_cat_obj = new  Main_Category;
						//Category table
						$main_cat_data_tb =$main_cat_obj->main_categ_view();

							if($main_cat_data_tb->num_rows >0){
								$x=1;
								while($main_categ_list=$main_cat_data_tb->fetch_assoc()){
									$main_categ_name=$main_categ_list['main_categ_name'];
									$main_categ_value=$main_categ_list['main_categ_folder'];
									
					?>
					<tr>
						<td style="vertical-align: middle;"><?php echo $x++; ?></td>
						<td style="font-size: 24px; vertical-align: middle;" >
							<?php echo $main_categ_name; ?>
						</td>
						<td>
							<table class="table table-bordered" style="margin-bottom:0px;">
							<?php
							############# Sub Category Table Start
								require_once('../class_lib/sub_category_class.php');
								$sub_categ_obj= new Sub_Category;
								$Sub_category_table=$sub_categ_obj->sub_categ_view_main($main_categ_value);
								
								if($Sub_category_table->num_rows > 0){
									while($sub_categ_list=$Sub_category_table->fetch_assoc()){
										$sub_categ_name=$sub_categ_list['sub_categ_name'];
										///print_r($sub_categ_list);
										?>
											<tr>
												<td><?php echo $sub_categ_name; ?></td>
											</tr>
										<?php
									}////// While Loop							
								}else{
								?>
								<tr>
									<td class="text-center">No Sub Category</td>
								</tr>
								<?php
								}
							?>
								
							</table>
						</td>
					</tr>
			<?php
			}///// main Category while loop
			
		}else{
			?>
					<tr>
						<td colspan="3" class="text-center" >Their have no category and sub category..</td>
					</tr>			
			
			<?php
		}
		
?>

					
				</table>
			</div>
		
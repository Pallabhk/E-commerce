<?php include('header.php');?>
<?php include('sidebar.php');?>

 	
	



<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">MAIN CATEGORY - ADD</span> || <a href="#">MAIN CATEGORY</a></h4>
			</div>	
		</div>
	</div>
</div>
<!-- Add about terms -->
	
  			<div class="container-fluid">
		    <!-- about basic info about module -->
			    <h3 class="alert alert-info text-center">Add Main Categories</h3>
			    <?php 
			   
				if (isset($_POST['main_categ_submit']) && !empty($_POST['main_categ_name'])) {
			    		require_once('../class_lib/maincatclass.php');

			    		$main_cat_obj = new Main_Category;
			    		$main_cat_obj->main_categ_insert($_POST);
			    	}
			   ?>
				<form method="POST" method="" class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
					
					<div class="form-group">
							<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
								<div class="row">
									
								<div class="col-md-8">
									 <div class="form-group">
										<label for="main_categ_name">Add Main Category</label>
										<input name="main_categ_name" type="text" class="form-control" id="main_categ_name"  placeholder="Add Main Category">
									 </div>
											  
									<div class="form-group">
										<button name="main_categ_submit" type="submit" class="btn btn-info">Add Main Category</button>
								    </div>
								</div>
							</div>
						</div>
					</div>

			   	</form>
		   </div>
		<br/>
	
	<div class="container-fluid">
		<h3 class="alert alert-success text-center">Add Sub Categories</h3>
			<?php
				###### Main Category View ########

				require_once('../class_lib/maincatclass.php');

				$main_cat_obj = new Main_Category;
				///sub category form
				$main_cat_data =$main_cat_obj->main_categ_view();
				//Category table
				$main_cat_data_tb =$main_cat_obj->main_categ_view();

				//Sub Category Insert 

				if (isset($_POST['sub_categ_submit']) && !empty($_POST['main_categ']) && !empty($_POST['sub_categ_name'])) {
					
					require_once('../class_lib/sub_category_class.php');
					$sub_categ_obj= new Sub_Category;
					$sub_categ_obj->sub_categ_insert($_POST);
				}else{
					if (isset($_POST['sub_categ_submit'])) {
						echo '<div class="alert alert-warning text-center" role="alert">Please fill-up all sub category fields</div>';
						header('refresh:2');
					}
				}


			?>
			<form method="post" class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
					  <div class="form-group">
						<label for="main_categ">Choose Main Category</label>
						<select name="main_categ" class="form-control" id="main_categ">
							<option value="">Choose Main Category</option>
							<?php
								if ($main_cat_data->num_rows>0) {
									while ($main_cat_list=$main_cat_data->fetch_assoc()) {
										echo '<option value="'.$main_cat_list['main_categ_folder'].'">'.$main_cat_list['main_categ_name'].'</option>';
									}
								}else{
									echo '<option value="">There have no Main Category</option>';
								}
							?>
						</select>
					  </div>
					  <div class="form-group">
						<label for="sub_categ_name">Add sub Category</label>
						<input name="sub_categ_name" type="text" class="form-control" id="sub_categ_name"  placeholder="Add Sub Category">
					  </div>
					  <div class="form-group">
					  <button name="sub_categ_submit" type="submit" class="btn btn-primary">Add Sub Category</button>
					  </div>
			</form>					
	</div>
		
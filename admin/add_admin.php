<?php
	 include('header.php');

	 if (!$_SESSION['r_admin_name']) {
	 	header('Location: index.php');

	 }
 ?>

<?php  include('sidebar.php');?>



<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">CREATE ADMIN - ADD</span> || <a href="#"> ADMIN</a></h4>
			</div>	
		</div>
	</div>
</div>
<!-- Add about terms -->

	
  			<div class="container-fluid">
		    <!-- about basic info about module -->
			    <h3 class="alert alert-info text-center">Add New Admin</h3>
			    <?php
					if (isset($_POST['n_submit']) && $_POST['n_pass'] ==$_POST['n_cpass'] && !empty($_POST['n_access'])) {
						
						require_once('../class_lib/add_admin_class.php');
						$admin_insert_obj = new Add_Admin;
						$admin_insert_obj->admin_insert($_POST);
					}else{
						if(isset($_POST['n_submit'])){
							echo '<div class="alert alert-warning text-center" role="alert">Information Invalid</div>';
							header('refresh:2; url=add_admin.php');
						}

					}
				?>
			   
				<form method="post" class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
	
				  
				  
				  <div class="form-group">
					<label for="n_access">Admin Access</label>
					<select name="n_access" class="form-control" id="n_access">
						<option value="">Choose Admin Access</option>
						<option value="admin">Admin</option>
						<option value="editor">Editor</option>
						<option value="operator">Operator</option>
					</select>
				  </div>
				  <div class="form-group">
					<label for="n_name">Admin Name</label>
					<input name="n_name" type="text" class="form-control" id="n_name"  placeholder="Admin Name">
				  </div>
				  <div class="form-group">
					<label for="n_email">Admin Email</label>
					<input name="n_email" type="email" class="form-control" id="n_email"  placeholder="Admin Email">
				  </div>
				  <div class="form-group">
					<label for="password">Password</label>
					<input name="n_pass" type="password" class="form-control" id="password" placeholder="Admin Password">
				  </div>
				  <div class="form-group">
					<label for="cpassword">Confirm Password</label>
					<input name="n_cpass" type="password" class="form-control" id="cpassword" placeholder="Confirm Password">
				  </div>
				  <button name="n_submit" type="submit" class="btn btn-success">Submit</button>
				</form>
		   </div>
		<br/>

<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"> | <a href="edit.php">Product List</a></span></h4>
			</div>
		</div>
	</div>
	<!-- View about options -->
</div> 	
<div class="container-fluid">
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10">
										<h2 class="text-center">About Product List</h2>
										 	
									</th>
								</tr>				
								<tr>
									<th>Sl Id</th>
									<th>Product NAme</th>
									<th>Admin Email</th>
									<th>Admin Status</th>
									<th>Manage</th>
								</tr>
							</thead>
							<tbody>
							
								<tr>
									<td>01</td>
									<td>Pallab</td>
									<td>pallabcse24@gmail.com</td>
									<td>root Admin</td>
									<td>
										<a class="btn-success" href="#">Edit</a>
										<a class="btn-danger" href="#">Delete</a> 
									</td>									
								</tr>
								<tr>
									<td>01</td>
									<td>Pallab</td>
									<td>pallabcse24@gmail.com</td>
									<td>root Admin</td>
									<td>
										<a class="btn-success" href="#">Edit</a>
										<a class="btn-danger" href="#">Delete</a> 
									</td>									
								</tr>
								<tr>
									<td>01</td>
									<td>Pallab</td>
									<td>pallabcse24@gmail.com</td>
									<td>root Admin</td>
									<td>
										<a class="btn-success" href="">Edit</a>
										<a class="btn-danger" href="">Delete</a> 
									</td>									
								</tr>
							
							</tbody>
						</table>
				</div>
		 </div>
	</div>				 
</div>
<?php 
	require_once('Database.php');

	/**
	* Add new Admin class 
	*/
	class Add_Admin extends DB_CONNECT{
		
		public function admin_insert($data){
			$admin_name=$data['n_name'];
			$admin_email=$data['n_email'];
			$admin_pass=$data['n_pass'];
			$admin_access=$data['n_access'];
			$error='';

			######## Admin Name Validation ############
			if(strlen($admin_name)>=6 && strlen($admin_name)<=50 && str_word_count($admin_name)>=2 && preg_match('/^[-a-zA-Z. ]*$/',$admin_name)){
				$success=1;
			}
			else{
				$error.= 'Full name must be with in 6-50 letters and 2 words minimum..<br>';
			}
			######## Admin Email Validation ############
			if(!filter_var($admin_email, FILTER_VALIDATE_EMAIL)){
				$error.= 'Invalid Email..<br>';
			}
			
			######## Admin Access Validation ############
			$ad_access=array('admin','editor','operator');
			if(!in_array($admin_access,$ad_access)){
				$error.= 'Invalid Admin Access<br>';
			}
			
			
			if(!$error){
				$db_connt=$this->connect;
				
				$sql_insert="INSERT INTO tbl_admin (admin_name, admin_email, admin_password, admin_action) VALUES ('$admin_name', '$admin_email', '$admin_pass', '$admin_access')";
				
				$result=$db_connt->query($sql_insert);
				if(!$result){
					echo '<div class="sr-only">'.$db_connt->error.'</div>';
					echo '<div class="alert alert-warning text-center" role="alert">Invalid Data ..</div>';
					header('refresh:3; url=add_admin.php');
				}else{
					echo '<div class="alert alert-success text-center" role="alert">Data Insert Successfully..</div>';
				}
				
			}else{
				echo '<div class="alert alert-warning text-center" role="alert">'.$error.'</div>';
			}
		
		}//insert method

		public function getAllAdmin($data){
			$fetch_query= "SELECT * FROM tbl_admin";
			$db_connt=$this->connect;
			$result =$db_connt->query($fetch_query);

			return $result;

		}
		
	}
?>
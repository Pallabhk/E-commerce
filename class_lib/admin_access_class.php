<?php

	require_once('Database.php');

	/**
	* Admin Access Class 
	*/
	class Admin_access extends DB_CONNECT
	{
		
		public function admin_login($data){

			$a_email     = $data['ad_email'];
			
			$a_password  = $data['ad_password'];

			$sql_check  ="SELECT * FROM tbl_admin WHERE admin_email='$a_email' && admin_password ='$a_password' ";

			$db_connect=$this->connect;

			$result =$db_connect->query($sql_check);

			if ($db_connect->error) {
				
				echo 'Error :' .$db_connect->error;


			}else{
				if($result->num_rows ==0) {
					echo '<div class="alert alert-warning text-center" role="alert">Invalid Email Or Password</div>';
					header('refresh:2 url=index.php');
						
				}else{

					$admin_data   =$result->fetch_assoc();
					$admin_name   =$admin_data['admin_name'];
					$admin_access =$admin_data['admin_action'];

					if ($admin_access == 'root_admin') {
						#####Cookies set ######
						if (isset($data['a_remb'])) {
							$ad_remb=$data['a_remb'];

							if ($ad_remb == 'Y') {
							 	setcookie('user_email',$a_email,time()+5*60 ,'/');
							 	setcookie('user_pass',$a_password,time()+5*60 ,'/');
							 } else{
							 	setcookie('user_email' ,time()+5*60 ,'/');
							 	setcookie('user_pass' ,time()+5*60 ,'/');
							 }
						}
						header('Location:root_admin.php');

						###Session Create ####
						$_SESSION['r_admin_name'] = $admin_name;
						header("Location:root_admin.php");

					}elseif ($admin_access == 'admin') {
						if (isset($data['a_remb'])) {
							$ad_remb =$data['a_remb'];
						if ($ad_remb == 'Y') {
							setcookie('user_email',$a_email,time()+5*60 ,'/');
							setcookie('user_pass',$a_password,time()+5*60 ,'/');
							}
						}else{
							setcookie('user_email' ,time()+5*60 ,'/');
							setcookie('user_pass' ,time()+5*60 ,'/');
						}
						header('Location:admin.php');

						###Session Create ####

						$_SESSION['admin_name'] =$admin_name;
						$_SESSION['action']     =$admin_access;
						header("Location:admin.php");
					}
				}
				
			}
		}//Admin Login 

		public function admin_logout(){

			session_unset();
			session_destroy();
			header('Location: index.php');
		}


	}//Class 
?>